# Installing spider

- [Prerequisites](./prerequisites)

## Prerequisites

Install into your SASS projects.

```
npm install @elioway/spider
yarn add @elioway/spider
```

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/spider.git
```
