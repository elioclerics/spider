"use strict"
const mongoose = require("mongoose")

const Product = new mongoose.Schema()

Product.plugin(require("./plugins/product"))
Product.plugin(require("./plugins/thing"))
Product.plugin(require("../../../adon"))

module.exports = mongoose.model("Product", Product)
