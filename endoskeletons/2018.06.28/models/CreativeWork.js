"use strict"
const mongoose = require("mongoose")

const CreativeWork = new mongoose.Schema()

CreativeWork.plugin(require("./plugins/creativework"))
CreativeWork.plugin(require("./plugins/thing"))
CreativeWork.plugin(require("../../../adon"))

module.exports = mongoose.model("CreativeWork", CreativeWork)
