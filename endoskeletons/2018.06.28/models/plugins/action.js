"use strict"
let mongoose = require("mongoose")
module.exports = exports = function action(schema, options) {
  schema.add({
    actionStatus: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ActionStatusType",
    },
    agent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    endTime: {
      type: Date,
    },
    error: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    instrument: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    location: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    object: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    participant: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    result: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    startTime: {
      type: Date,
    },
    target: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "EntryPoint",
    },
  })
}
