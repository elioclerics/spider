"use strict"
let mongoose = require("mongoose")
module.exports = exports = function product(schema, options) {
  schema.add({
    additionalProperty: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PropertyValue",
    },
    aggregateRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AggregateRating",
    },
    audience: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Audience",
    },
    award: {
      type: String,
    },
    brand: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Brand",
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PhysicalActivityCategory",
    },
    color: {
      type: String,
    },
    depth: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Distance",
    },
    gtin12: {
      type: String,
    },
    gtin13: {
      type: String,
    },
    gtin14: {
      type: String,
    },
    gtin8: {
      type: String,
    },
    height: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Distance",
    },
    isAccessoryOrSparePartFor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    isConsumableFor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    isRelatedTo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    isSimilarTo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    itemCondition: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OfferItemCondition",
    },
    logo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ImageObject",
    },
    manufacturer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    material: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    model: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ProductModel",
    },
    mpn: {
      type: String,
    },
    offers: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Offer",
    },
    productID: {
      type: String,
    },
    productionDate: {
      type: Date,
    },
    purchaseDate: {
      type: Date,
    },
    releaseDate: {
      type: Date,
    },
    review: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review",
    },
    sku: {
      type: String,
    },
    weight: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "QuantitativeValue",
    },
    width: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Distance",
    },
  })
}
