"use strict"
let mongoose = require("mongoose")
module.exports = exports = function event(schema, options) {
  schema.add({
    about: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    actor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    aggregateRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AggregateRating",
    },
    attendee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    audience: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Audience",
    },
    composer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    contributor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    director: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    doorTime: {
      type: Date,
    },
    duration: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Duration",
    },
    endDate: {
      type: Date,
    },
    eventStatus: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "EventStatusType",
    },
    funder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    inLanguage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Language",
    },
    isAccessibleForFree: {
      type: Boolean,
    },
    location: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    maximumAttendeeCapacity: {
      type: Number,
    },
    offers: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Offer",
    },
    organizer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    performer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    previousStartDate: {
      type: Date,
    },
    recordedIn: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    remainingAttendeeCapacity: {
      type: Number,
    },
    review: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review",
    },
    sponsor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    startDate: {
      type: Date,
    },
    subEvent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    superEvent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    translator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    typicalAgeRange: {
      type: String,
    },
    workFeatured: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    workPerformed: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
  })
}
