"use strict"
let mongoose = require("mongoose")
module.exports = exports = function organization(schema, options) {
  schema.add({
    actionableFeedbackPolicy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    address: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PostalAddress",
    },
    aggregateRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AggregateRating",
    },
    alumni: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    areaServed: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AdministrativeArea",
    },
    award: {
      type: String,
    },
    brand: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Brand",
    },
    contactPoint: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ContactPoint",
    },
    correctionsPolicy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    department: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    dissolutionDate: {
      type: Date,
    },
    diversityPolicy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    diversityStaffingReport: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Article",
    },
    duns: {
      type: String,
    },
    email: {
      type: String,
    },
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    ethicsPolicy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    event: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    faxNumber: {
      type: String,
    },
    founder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    foundingDate: {
      type: Date,
    },
    foundingLocation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    funder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    globalLocationNumber: {
      type: String,
    },
    hasOfferCatalog: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OfferCatalog",
    },
    hasPOS: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    isicV4: {
      type: String,
    },
    knowsAbout: {
      type: String,
    },
    knowsLanguage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Language",
    },
    legalName: {
      type: String,
    },
    leiCode: {
      type: String,
    },
    location: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    logo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ImageObject",
    },
    makesOffer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Offer",
    },
    member: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    memberOf: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    naics: {
      type: String,
    },
    numberOfEmployees: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "QuantitativeValue",
    },
    ownershipFundingInfo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AboutPage",
    },
    owns: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OwnershipInfo",
    },
    parentOrganization: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    publishingPrinciples: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    review: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review",
    },
    seeks: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Demand",
    },
    sponsor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    subOrganization: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    taxID: {
      type: String,
    },
    telephone: {
      type: String,
    },
    unnamedSourcesPolicy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    vatID: {
      type: String,
    },
  })
}
