"use strict"
let mongoose = require("mongoose")
module.exports = exports = function person(schema, options) {
  schema.add({
    additionalName: {
      type: String,
    },
    address: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PostalAddress",
    },
    affiliation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    alumniOf: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "EducationalOrganization",
    },
    award: {
      type: String,
    },
    birthDate: {
      type: Date,
    },
    birthPlace: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    brand: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Brand",
    },
    children: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    colleague: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    contactPoint: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ContactPoint",
    },
    deathDate: {
      type: Date,
    },
    deathPlace: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    duns: {
      type: String,
    },
    email: {
      type: String,
    },
    familyName: {
      type: String,
    },
    faxNumber: {
      type: String,
    },
    follows: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    funder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    gender: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GenderType",
    },
    givenName: {
      type: String,
    },
    globalLocationNumber: {
      type: String,
    },
    hasOccupation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Occupation",
    },
    hasOfferCatalog: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OfferCatalog",
    },
    hasPOS: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    height: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Distance",
    },
    homeLocation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ContactPoint",
    },
    honorificPrefix: {
      type: String,
    },
    honorificSuffix: {
      type: String,
    },
    isicV4: {
      type: String,
    },
    jobTitle: {
      type: String,
    },
    knows: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    knowsAbout: {
      type: String,
    },
    knowsLanguage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Language",
    },
    makesOffer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Offer",
    },
    memberOf: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    naics: {
      type: String,
    },
    nationality: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Country",
    },
    netWorth: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "MonetaryAmount",
    },
    owns: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OwnershipInfo",
    },
    parent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    performerIn: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    publishingPrinciples: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    relatedTo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    seeks: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Demand",
    },
    sibling: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    sponsor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    spouse: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    taxID: {
      type: String,
    },
    telephone: {
      type: String,
    },
    vatID: {
      type: String,
    },
    weight: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "QuantitativeValue",
    },
    workLocation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ContactPoint",
    },
    worksFor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
  })
}
