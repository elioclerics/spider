"use strict"
let mongoose = require("mongoose")
module.exports = exports = function creativework(schema, options) {
  schema.add({
    about: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    accessMode: {
      type: String,
    },
    accessModeSufficient: {
      type: String,
    },
    accessibilityAPI: {
      type: String,
    },
    accessibilityControl: {
      type: String,
    },
    accessibilityFeature: {
      type: String,
    },
    accessibilityHazard: {
      type: String,
    },
    accessibilitySummary: {
      type: String,
    },
    accountablePerson: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    aggregateRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AggregateRating",
    },
    alternativeHeadline: {
      type: String,
    },
    associatedMedia: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "MediaObject",
    },
    audience: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Audience",
    },
    audio: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AudioObject",
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    award: {
      type: String,
    },
    character: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    citation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    comment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment",
    },
    commentCount: {
      type: Number,
    },
    contentLocation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    contentRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Rating",
    },
    contentReferenceTime: {
      type: Date,
    },
    contributor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    copyrightHolder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    copyrightYear: {
      type: Number,
    },
    correction: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CorrectionComment",
    },
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    dateCreated: {
      type: Date,
    },
    dateModified: {
      type: Date,
    },
    datePublished: {
      type: Date,
    },
    discussionUrl: {
      type: String,
    },
    editor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
    educationalAlignment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AlignmentObject",
    },
    educationalUse: {
      type: String,
    },
    encoding: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "MediaObject",
    },
    encodingFormat: {
      type: String,
    },
    exampleOfWork: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    expires: {
      type: Date,
    },
    funder: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    genre: {
      type: String,
    },
    hasPart: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    headline: {
      type: String,
    },
    inLanguage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Language",
    },
    interactionStatistic: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "InteractionCounter",
    },
    interactivityType: {
      type: String,
    },
    isAccessibleForFree: {
      type: Boolean,
    },
    isBasedOn: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    isFamilyFriendly: {
      type: Boolean,
    },
    isPartOf: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    keywords: {
      type: String,
    },
    learningResourceType: {
      type: String,
    },
    license: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    locationCreated: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    mainEntity: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    material: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
    },
    mentions: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Thing",
    },
    offers: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Offer",
    },
    position: {
      type: Number,
    },
    producer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    provider: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    publication: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PublicationEvent",
    },
    publisher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    publisherImprint: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    publishingPrinciples: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    recordedAt: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    releasedEvent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PublicationEvent",
    },
    review: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review",
    },
    schemaVersion: {
      type: String,
    },
    sdDatePublished: {
      type: Date,
    },
    sdLicense: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    sdPublisher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    sourceOrganization: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    spatialCoverage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    sponsor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    temporalCoverage: {
      type: Date,
    },
    text: {
      type: String,
    },
    thumbnailUrl: {
      type: String,
    },
    timeRequired: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Duration",
    },
    translationOfWork: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    translator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Organization",
    },
    typicalAgeRange: {
      type: String,
    },
    version: {
      type: Number,
    },
    video: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "VideoObject",
    },
    workExample: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    workTranslation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
  })
}
