"use strict"
const mongoose = require("mongoose")

const Person = new mongoose.Schema()

Person.plugin(require("./plugins/person"))
Person.plugin(require("./plugins/thing"))
Person.plugin(require("../../../adon"))

module.exports = mongoose.model("Person", Person)
