"use strict"
const mongoose = require("mongoose")

const Place = new mongoose.Schema()

Place.plugin(require("./plugins/place"))
Place.plugin(require("./plugins/thing"))
Place.plugin(require("../../../adon"))

module.exports = mongoose.model("Place", Place)
