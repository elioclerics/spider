"use strict"
const mongoose = require("mongoose")

const Thing = new mongoose.Schema()

Thing.plugin(require("./plugins/thing"))

// no adon on a shoestring
// Thing.plugin(require('../../../adon'))

// try str with lowercase on a shoestring
module.exports = mongoose.model("thing", Thing)
